import cherrypy

import Auth
import User


if __name__ == '__main__':

	generic_conf = {
		'/': {
			'request.dipatch': cherrypy.dispatch.MethodDispatcher(),
			'tools.response_headers.on': True,
			'tools.reponse_headers.headers': [('Content-Type','text/plain')]
		}
	}

	cherrypy.tree.mount(Auth.Auth(),'/',generic_conf)
	cherrypy.tree.mount(User.User(),'/',generic_conf)
	cherrypy.tree.mount(User.Find(),'/User',generic_conf)

	cherrypy.engine.start()
	cherrypy.engine.block()